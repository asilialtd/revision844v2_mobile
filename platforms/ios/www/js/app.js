$(function(){
	var api_base_url = 'http://www.revision844.co.ke/';
	var profile, set_item, chosen, progress;

	//set hrefs for reset password and resend verification link
	// $('a.recover').attr('href', api_base_url+'secret/new');
	// $('a.reverify').attr('href', api_base_url+'verification/new');
	//take out screen
	function takeOut(view){
		view.hide("slide", {direction: "left"}, 500);
	}
	//bring in screen
	function bringIn(view){
		view.show("slide", {direction: "right"}, 500);
	}
	//empty html
	function emptyHtml(element){
		element.html('');
	}
	//ajax loading presets
	var $loading = $('.loading');
	$(document).ajaxStart(function(){
		$loading.show();
	}).ajaxStop(function(){
		$loading.hide();
	});
	//nav click
	$('.navbar').on('click', '.collapse.in .nav a', function(){
		$('.navbar-toggle').click();
	});
	//add stuff
	function addItem(item, item_data){
		switch(item){
			case 'class':
				$('#classes .list').append('<div class="classroom item" data-id="'+item_data.id+'"><span class="item-name">'+item_data.name+'</span></div>');
			break;
			case 'sub':
				$('#subjects .list').append('<div class="subject item" data-id="'+item_data.id+'"><span class="item-name">'+item_data.name+'</span></div>');
			break;
			case 'set':
				html = '<div class="set item" data-id="'+item_data.id+'"><div class="item-name dark-grey">'+item_data.name;
				if(item_data.premium){
					html += ' <br><span class="badge">Premium</span>';
				}
				html += '</div><div class="more action">More <span class="glyphicon glyphicon-info-sign"></span></div>';
				if(item_data.person_set === 0){
					if(item_data.entities_count > 0){
						html += '<div class="play action">Start <span class="glyphicon glyphicon-play"></span></div>';
					}
				}else{
					if(item_data.entities_count > 0){
						if(item_data.person_set.marked){
							html += '<div class="play action">Review <span class="glyphicon glyphicon-play"></span></div>';
						}else{
							html += '<div class="play action">Continue <span class="glyphicon glyphicon-play"></span></div>';
						}
						html += '<div class="restart action">Restart <span class="glyphicon glyphicon-repeat"></span></div>';
					}
				}
				
				html += '</div>';
				$('#sets .list').append(html);
			break;
			case 'myset':
				html = '';
				html += '<div class="set item" data-id="'+item_data.collection.id+'"><div class="item-name">'+item_data.collection.name;
				if(item_data.collection.premium){
					html += ' <br><span class="badge">Premium</span>';
				}
				html +=	'</div><div class="more action">More <span class="glyphicon glyphicon-info-sign"></span></div>';
				if(item_data.collection.entities_count > 0){
					if(item_data.marked){
						html += '<div class="play action">Review <span class="glyphicon glyphicon-play"></span>';
					}else{
						html += '<div class="play action">Continue <span class="glyphicon glyphicon-play"></span>';
					}
						html += '</div><div class="restart action">Restart <span class="glyphicon glyphicon-repeat"></span></div>';
				}

				html += '</div>';
				$('#mysets .list').append(html);
			break;
			case 'entity':
				html = '<div class="entity" data-id="'+item_data.entity.id+'"><div class="entity-title"><span class="item-text">'+item_data.entity.entity_text.replace(/\n/g,"<br>")+'</span></div>';
				html += '<div class="meta">';
				//add graphic
				if(item_data.entity.graphic){
					html += '<div class="graphic"><img class="img-responsive" src="'+item_data.entity.graphic+'" alt="Image"></div>';
				}
				//add embed
				if(item_data.entity.embed_code){
					html += '<div class="embed">'+item_data.entity.embed_code+'</div>';
				}
				html += '</div>';
				//question specifics
				if(item_data.entity.format === "question"){
					html += '<div class="list"><div class="options">';
					//options
					if(item_data.entity.option_a){
						html += '<div class="option" data-value="A"><span class="choice">A</span> '+item_data.entity.option_a+'</div>';
					}
					if(item_data.entity.option_b){
						html += '<div class="option" data-value="B"><span class="choice">B</span> '+item_data.entity.option_b+'</div>';
					}
					if(item_data.entity.option_c){
						html += '<div class="option" data-value="C"><span class="choice">C</span> '+item_data.entity.option_c+'</div>';
					}
					if(item_data.entity.option_d){
						html += '<div class="option" data-value="D"><span class="choice">D</span> '+item_data.entity.option_d+'</div>';
					}
					html += '</div></div>';
				}
				//add comment
				html += '<div class="comment text-muted">';
				if(item_data.entity.format == 'note' && item_data.entity.comment){
					html += item_data.entity.comment.replace(/\n/g,"<br>");
				}
				html += '</div></div>';
				
				$('#entity .entities').append(html);
			break;
			case 'set-profile':
				$('#set-info h1').html(item_data.name);
				$('#set-info .subject').html('<span class="glyphicon glyphicon-tag"></span> '+item_data.subject.name);
				$('#set-info .desc').html('<span class="glyphicon glyphicon-pencil"></span> '+item_data.description);
				$('#set-info .entities').html('<span class="glyphicon glyphicon-stats"></span> '+ item_data.entities_count);
				if(item_data.premium === false){ $('#set-info .premium').html('<span class="glyphicon glyphicon-lock"></span> No');}
				if(item_data.premium === true){ $('#set-info .premium').html('<span class="glyphicon glyphicon-lock"></span> Yes');}
				$('#set-info .created').html('<span class="glyphicon glyphicon-calendar"></span> '+item_data.created_on);
			break;
			case 'me-profile':
				name = '';
				if(item_data.first_name && item_data.last_name){
					name = item_data.first_name+' '+item_data.last_name; 
				}else{
					if(item_data.first_name){
						name = item_data.first_name;
					}else if(item_data.last_name){
						name = item_data.last_name;
					}else{
						name = 'Me';
					}
				}
				$('#me h1').text(name);
				$('#me .email').html(item_data.email);
				$('#me .sets').html('<img class="info-ico" src="images/icon-sets.png" alt="Sets icon"> <span class="detail">'+item_data.person_sets_count+' Sets</span>');
				if(item_data.customer === false){ $('#me .paying').html('<img class="info-ico" src="images/icon-free-plan.png" alt="Free plan icon"> <span class="detail"> FREE Plan</span> &middot; <button type="button" class="btn btn-success caps" data-toggle="modal" data-target="#upgrade_modal">Upgrade</button>');}
				if(item_data.customer === true){ $('#me .paying').html('<img class="info-ico" src="images/icon-premium-plan.png" alt="Premium plan icon"> <span class="detail"> PREMIUM Plan</span>'); }
				$('#me .created').html('<img class="info-ico" src="images/icon-calendar.png" alt="Calendar icon"> <span class="detail">Joined on</span> '+item_data.created_on);
			break;
		}
	}
	//load class subjects
	$('#classes .list').on('click', '.item', function(){
		class_id = $(this).data('id');
		$('#subjects h1').html($(this).find('.item-name').text()+' <span class="crumb">&gt;</span> <span class="green">Subjects</span>');
		emptyHtml($('#subjects .list'));

		$.getJSON(api_base_url+'classrooms/'+class_id+'/subjects.json?key='+login.auth_token, function(subjects){
			$.each(subjects, function(){ addItem('sub', this); });
		});

		takeOut($('#classes'));
		bringIn($('#subjects'));
	});
	//load subject sets
	$('#subjects .list').on('click', '.item', function(){
		subject_id = $(this).data('id');
		$('#sets h1').html($(this).find('.item-name').text()+' Sets</span>');
		emptyHtml($('#sets .list'));

		$.getJSON(api_base_url+'subjects/'+subject_id+'/sets.json?key='+login.auth_token, function(sets){
			$.each(sets, function(){ addItem('set', this); });
		});

		takeOut($('#subjects'));
		bringIn($('#sets'));
	});
	//load set details
	$('#sets .list, #mysets .list').on('click', '.more', function(){
		set_id = $(this).parents('div').eq(0).data('id');
		emptyHtml($('#set-info .list .field'));

		//load set profile
		$.getJSON(api_base_url+'sets/'+set_id+'.json?key='+login.auth_token, function(set){
			addItem('set-profile', set);
		});

		//manage back navigation
		$('#set-info .btn-back').hide();
		view = $(this).parents('.screen').eq(0).attr('id');
		if(view == 'mysets'){ $('#set-info .back-my-sets').show();}
		if(view == 'sets'){ $('#set-info .back-sets').show();}

		//transition screen
		takeOut($('.screen'));
		bringIn($('#set-info'));
	});
	//restart set
	$('#sets .list, #mysets .list').on('click', '.restart', function(e){
		$('.progress-wrapper').hide();
		$('#entity .scoresheet').hide();
		set_id = $(this).parents('div').eq(0).data('id');
		view = $(this).parents('.screen').eq(0).attr('id');

		//restart set
		$.get(api_base_url+'people/'+login.person_id+'/sets/'+set_id+'/restart?key='+login.auth_token, 'person_set[collection_id]='+set_id, function(entity){
			if(entity.success){
				//bootstrap
				chosen = {};
				progress = 0;
				set_item = entity;
				emptyHtml($('#entity .entities'));

				//add items
				$('#entity .page-title').html(set_item.collection.name);

				$.each(set_item.entities, function(){ 
					addItem('entity', this); 
				});

				//progressbar
				total = $('.options').length;
				$('#entity .progress-bar').attr("aria-valuemax", total);
				$('#entity .progress-bar').attr("aria-valuenow", progress);
				$('#entity .progress-bar').css({"width": (progress/total * 100)+"%"});
				$('#entity .complete').html(progress+'/'+ total);
				
				//make selections possible
				$('#entity .entities').on('click', '.option', optionSelect);

				takeOut($('.screen'));
				bringIn($('#entity'));
			}else{
				set_item = {};
				if(view == 'mysets'){
					$('#upgrade_modal').modal({ show: false});
					$('#upgrade_modal').modal('show');
					scrollToItem($('#mysets'));
				}
				if(view == 'sets'){
					$('#upgrade_modal').modal({ show: false});
					$('#upgrade_modal').modal('show');
					scrollToItem($('#sets'));
				}
			}
		});

		//manage back navigation
		$('#entity .btn-back').hide();
		$('#entity .btn-next').show();
		view = $(this).parents('.screen').eq(0).attr('id');
		if(view == 'mysets'){ $('#entity .back-my-sets').show();}
		if(view == 'sets'){ $('#entity .back-sets').show();}
	});

	//play set
	$('#sets .list, #mysets .list').on('click', '.play', function(e){
		$('.progress-wrapper').hide();
		set_id = $(this).parents('div').eq(0).data('id');
		view = $(this).parents('.screen').eq(0).attr('id');

		//start or resume set
		$.post(api_base_url+'people/'+login.person_id+'/sets/play?key='+login.auth_token, 'person_set[collection_id]='+set_id, function(entity){
			if(entity.success){
				//bootstrap
				chosen = {};
				progress = 0;
				set_item = entity;
				emptyHtml($('#entity .entities'));

				//add items
				$('#entity .page-title').html(set_item.collection.name);

				$.each(set_item.entities, function(){ 
					addItem('entity', this); 
				});

				//show previous selections
				chosen = JSON.parse(set_item.chosen);

				$.each(chosen, function(key, value){
					$('.entity[data-id="'+key+'"] .option[data-value="'+value+'"]').addClass('selected');
				});
				//show progress bar
				progress = set_item.progress;
				if(progress > 0){
					$('.progress-wrapper').show();
				}

				if(set_item.collection.entities_count > 0){
					total = $('.options').length;
					$('#entity .progress-bar').attr("aria-valuemax", total);
					$('#entity .progress-bar').attr("aria-valuenow", progress);
					$('#entity .progress-bar').css({"width": (progress/total * 100)+"%"});
					$('#entity .complete').html(progress+'/'+total);

					//if in marked stage
					if(set_item.marked){
						$('#entity .entities').off('click', '.option', optionSelect);
						$('#entity .entities .option').css({'cursor':'default'});

						$.each(set_item.entities, function(){
							$('.entity[data-id="'+this.entity.id+'"] .option[data-value="'+this.entity.question_answer+'"]').addClass('right');
							$('.entity[data-id="'+this.entity.id+'"] .comment').html(this.entity.comment.replace(/\n/g,"<br>"));
							if(chosen[this.entity.id] != this.entity.question_answer){
								$('.entity[data-id="'+this.entity.id+'"] .selected').addClass('wrong');
							}
						});
						showScore();
					}else{
						$('#entity .entities').on('click', '.option', optionSelect);
						$('#entity .scoresheet').hide();
					}
				}else{
					total = 0;
				}


				takeOut($('.screen'));
				bringIn($('#entity'));
			}else{
				set_item = {};
				if(view == 'mysets'){
					$('#upgrade_modal').modal({ show: false});
					$('#upgrade_modal').modal('show');
					scrollToItem($('#mysets'));
				}
				if(view == 'sets'){
					$('#upgrade_modal').modal({ show: false});
					$('#upgrade_modal').modal('show');
					scrollToItem($('#sets'));
				}
			}
		});

		//manage back navigation
		$('#entity .btn-back').hide();
		$('#entity .btn-next').show();
		view = $(this).parents('.screen').eq(0).attr('id');
		if(view == 'mysets'){ $('#entity .back-my-sets').show();}
		if(view == 'sets'){ $('#entity .back-sets').show();}
	});

	//advance in a set (next)
	$('#entity .btn-next').click(function(e){
		//submit choosen answers
		$.ajax({
			type:"POST",
			url:api_base_url+'people/'+login.person_id+'/sets/'+set_item.id+'/next?key='+login.auth_token,
			data:{ _method:"PUT", person_set:{chosen: JSON.stringify(chosen), progress: progress} }
		}).done(function(response){
			if(response.success){
				$('#entity .alert').text(response.message);
				$('#entity .alert').attr('class', 'alert alert-success').slideDown().delay(5000).slideUp();
				//show answers and hints
				$.each(response.entities, function(){
					$('.entity[data-id="'+this.entity.id+'"] .option[data-value="'+this.entity.question_answer+'"]').addClass('right');
					$('.entity[data-id="'+this.entity.id+'"] .comment').html(this.entity.comment);
					if(chosen[this.entity.id] != this.entity.question_answer){
						$('.entity[data-id="'+this.entity.id+'"] .selected').addClass('wrong');
					}
				});
				showScore();
			}else{
				$('#entity .alert').text(response.message);
				$('#entity .alert').attr('class', 'alert alert-danger').slideDown().delay(5000).slideUp();
			}
		}).fail(function(response){
			//failed - do nothing or something

		});
	});
	
	//login
	$('#btn-login').click(function(e){
		$('#btn-login').prop('disabled', true);
		if(!$('#login .required').val()){
			$('#login .alert').text('Please enter your email and password and try again');
			$('#login .alert').attr('class', 'alert alert-danger').slideDown().delay(5000).slideUp();
		}else{
			$.ajax({
				type:"POST",
				url:api_base_url+'api/people/sign_in',
				data: $('#frm-signin').serialize()
			}).done(function(response){
				//show classes
				if(response.success === true){
					login = response;

					//store stuff
					if(window.localStorage){
						localStorage.setItem('auth_token', response.auth_token);
						localStorage.setItem('email', response.email);
						localStorage.setItem('customer', response.customer);
						localStorage.setItem('person_sets_count', response.person_sets_count);
						localStorage.setItem('person_id', response.person_id);
					}

					onLogin(login);
					//clear form and go next
					$('#login .required').val('');
				}else{
					if(response.message){
						$('#login .alert').html(response.message);
						$('#login .alert').attr('class', 'alert alert-danger').slideDown().delay(5000).slideUp();
					}
				}
			}).fail(function(response){
				//failed - do nothing or something
				$('#login .alert').html("Uh-Oh, something went wrong. Please try again. If you recently signed up, please confirm your account if you haven't yet.");
				$('#login .alert').attr('class', 'alert alert-danger').slideDown().delay(5000).slideUp();
			});
		}
		$('#btn-login').prop('disabled', false);
		e.preventDefault();
	});
	//signup
	$('#btn-signup').click(function(e){
		$('#btn-signup').prop('disabled', true);
		if(!$('#signup .required').val()){
			$('#signup .alert').text('Please fill in all the fields above and try again.');
			$('#signup .alert').attr('class', 'alert alert-danger').slideDown().delay(5000).slideUp();
		}else{
			$.ajax({
				type:"POST",
				url: api_base_url+'api/people',
				data: $('#frm-signup').serialize()
			}).done(function(response){
				//go to login page
				if(response.success === true){
					$('#signup .required').val('');
					$('#login #email').val(response.email);
					$('#login .alert').html('Please verify your account by clicking the link emailed to you then come back and enter your password to get started.');
					$('#login .alert').attr('class', 'alert alert-success').slideDown().delay(10000).slideUp();
					takeOut($('#signup'));
					bringIn($('#login'));
				}else{
					alert_text = '';
					if(response.errors.email){ alert_text += 'Email '+response.errors.email+'<br>';}
					if(response.errors.password){ alert_text += 'Password '+response.errors.password+'<br';}
					if(response.errors.password_confirmation){ alert_text += 'Password confirmation '+response.errors.password_confirmation+'<br>';}
					//set and reveal
					$('#signup .alert').html(alert_text);
					$('#signup .alert').attr('class', 'alert alert-danger').slideDown().delay(5000).slideUp();
				}
			}).fail(function(response){
				//failed - do nothing or something
				$('#signup .alert').html("Uh-Oh, something went wrong. Please try again");
				$('#signup .alert').attr('class', 'alert alert-danger').slideDown().delay(5000).slideUp();
			});
		}
		$('#btn-signup').prop('disabled', false);
		e.preventDefault();
	});
	//go to signup
	$('#login .btn-signup').click(function(e){
		takeOut($('#login'));
		bringIn($('#signup'));
		e.preventDefault();
	});
	//go to login
	$('#signup .btn-login').click(function(e){
		takeOut($('#signup'));
		bringIn($('#login'));
		e.preventDefault();
	});
	//logout
	$('.nav-out').click(function(e){
		if($('#entity').is(':visible')){
			syncSelections();
		}
		$.ajax({
			type:"POST",
			url:api_base_url+'api/people/sign_out',
			data: { _method:"DELETE", person:{key: login.auth_token}}
		}).done(function(response){
			if(response.success === true){
				//clear storage
				localStorage.removeItem('auth_token');
				localStorage.removeItem('email');
				localStorage.removeItem('customer');
				localStorage.removeItem('person_sets_count');
				localStorage.removeItem('person_id');

				//update UI
				takeOut($('.screen'));
				$('.navbar-nav').fadeOut();
				$('.navbar-toggle').fadeOut();
				bringIn($('#login'));
				$('#classes .list').html('');
			}
		}).fail(function(response){
			//to do
		});

		e.preventDefault();
	});
	//show my sets
	$('.nav-sets').click(function(e){
		$.getJSON(api_base_url+'people/'+login.person_id+'/sets.json?key='+login.auth_token, function(sets){
			$.each(sets, function(){ addItem('myset', this); });
		});
		
		takeOut($('.screen'));
		bringIn($('#mysets'));

		$('.navbar').find('li.active').removeClass('active');
		$(this).parents('li').eq(0).addClass('active');

		if(progress > 0){
			syncSelections();
		}
		emptyHtml($('#mysets .list'));

		e.preventDefault();
	});
	//show me profile
	$('.nav-me').click(function(e){
		emptyHtml($('#me .field'));
		$.getJSON(api_base_url+'people/'+login.person_id+'.json?key='+login.auth_token, function(me){
			addItem('me-profile', me);
			profile = me;
		});

		takeOut($('.screen'));
		bringIn($('#me'));

		$('.navbar').find('li.active').removeClass('active');
		$(this).parents('li').eq(0).addClass('active');

		if(progress > 0){
			syncSelections();
		}

		e.preventDefault();
	});
	//show account settings
	$('.btn-account').click(function(e){
		$('#account #first_name').val(profile.first_name);
		$('#account #last_name').val(profile.last_name);
		$('#account #email').val(profile.email);
		$('#account #password').val('');
		$('#account #password_confirmation').val('');
		$('#account #current_password').val('');

		takeOut($('#me'));
		bringIn($('#account'));

		e.preventDefault();
	});
	//update account details
	$('#btn-edit-account').click(function(e){
		if(!$('#frm-edit-account .required').val()){
			$('#account #edit-alert').text('Enter your current password to update your account.');
			$('#account #edit-alert').attr('class', 'alert alert-danger').slideDown().delay(5000).slideUp();
		}else{
			$.ajax({
				type:"POST",
				url:api_base_url+'api/people/?key='+login.auth_token,
				data: $('#frm-edit-account').serialize()
			}).done(function(response){
				if(response.success){
					$('#account #edit-alert').text('Your account has been updated.');
					$('#account #edit-alert').attr('class', 'alert alert-success').slideDown().delay(5000).slideUp();
				}else{
					alert_text = '';
					if(response.errors.current_password){ alert_text += 'Current password '+response.errors.current_password+'<br>';}
					if(response.errors.first_name){ alert_text += 'First name'+response.errors.first_name+'<br>';}
					if(response.errors.last_name){ alert_text += 'Last name'+response.errors.last_name+'<br>';}
					if(response.errors.email){ alert_text += 'Email'+response.errors.email+'<br>';}
					if(response.errors.password){ alert_text += 'Password'+response.errors.password+'<br>';}
					if(response.errors.password_confirmation){ alert_text += 'Password confirmation'+response.errors.password_confirmation+'<br>';}
					//set and reveal
					$('#account #edit-alert').html(alert_text);
					$('#account #edit-alert').attr('class', 'alert alert-danger').slideDown().delay(5000).slideUp();
				}
			}).fail(function(response){
				$('#account #edit-alert').text("Your update was not saved. Please try again.");
				$('#account #edit-alert').attr('class', 'alert alert-danger').slideDown().delay(5000).slideUp();
			});
		}

		e.preventDefault();
	});
	//cancel account
	$('#btn-cancel-account').click(function(e){
		if(!$('#frm-cancel-account .required').val()){
			$('#account #cancel-alert').text('Enter your current password to cancel your account.');
			$('#account #cancel-alert').attr('class', 'alert alert-danger').slideDown().delay(5000).slideUp();
		}else{
			$.ajax({
				type:"POST",
				url:api_base_url+'api/people/?key='+login.auth_token,
				data: $('#frm-cancel-account').serialize()
			}).done(function(response){
				if(response.success === true){
					$('#login .alert').text("We're sad you've left. We hope to see you again soon!");
					takeOut($('.screen'));
					$('.navbar-nav').fadeOut();
					$('.navbar-toggle').fadeOut();
					bringIn($('#login'));
					$('#classes .list').html('');
					$('#login .alert').attr('class', 'alert alert-info').slideDown().delay(10000).slideUp();
				}else{
					$('#accoount #cancel-alert').text(response.errors.current_password);
					$('#account #cancel-alert').attr('class', 'alert alert-danger').slideDown().delay(5000).slideUp();
				}
			}).fail(function(response){
				$('#accoount #cancel-alert').text("Something went wrong. Your account was not cancelled.");
				$('#accoount #cancel-alert').attr('class', 'alert alert-danger').slideDown().delay(5000).slideUp();
			});
		}
		e.preventDefault();
	});
	//show home
	$('.nav-home').click(function(e){
		takeOut($('.screen'));
		bringIn($('#classes'));

		$('.navbar').find('li.active').removeClass('active');
		$(this).parents('li').eq(0).addClass('active');
		if(progress > 0){
			syncSelections();
		}
		e.preventDefault();
	});
	$('.navbar-brand').click(function(e){
		if(window.localStorage && localStorage.auth_token){
			$('.nav-home').click();
		}else{
			window.open('http://www.revision844.co.ke', '_blank');
		}
		e.preventDefault();
	});
	//back home
	$('body').on('click', '.back-home', function(){
		takeOut($(this).parents('.screen').eq(0));
		bringIn($('#classes'));
	});
	//back to subjects
	$('body').on('click', '.back-subjects', function(){
		takeOut($(this).parents('.screen').eq(0));
		bringIn($('#subjects'));
	});
	//back to sets
	$('body').on('click', '.back-sets', function(){
		takeOut($(this).parents('.screen').eq(0));
		bringIn($('#sets'));
		if(progress > 0){
			syncSelections();
		}
		progress = 0;
		
		$('.progress-wrapper').hide();
		$('.scoresheet').hide();
	});
	//back to my sets
	$('body').on('click', '.back-my-sets', function(){
		takeOut($(this).parents('.screen').eq(0));

		emptyHtml($('#mysets .list'));
		$.getJSON(api_base_url+'people/'+login.person_id+'/sets.json?key='+login.auth_token, function(sets){
			$.each(sets, function(){ addItem('myset', this); });
		});

		bringIn($('#mysets'));

		if(progress > 0){
			syncSelections();
		}
		progress = 0;
		$('.progress-wrapper').hide();
		$('.scoresheet').hide();
	});
	//back to profile
	$('body').on('click', '.back-me', function(e){
		takeOut($(this).parents('.screen').eq(0));
		bringIn($('#me'));
		e.preventDefault();
	});
	//submit payment code
	$('#submit_pay').on('click', function(e){
		emptyHtml($('#upgrade_alert'));
		$('#upgrade_alert').removeClass("alert-success alert-danger");
		receipt = $('#pay_code').val();

		if(receipt){
			$.getJSON(api_base_url+'subscriptions/status_v2.json?receipt='+receipt+'&user='+login.person_id, function(data){
				if(data.success){
					$('#upgrade_alert').addClass("alert-success").html(data.message).slideDown().delay(9000).slideUp();
					$('#pay_code').val('');
				}else{
					$('#upgrade_alert').addClass("alert-danger").html(data.error).slideDown().delay(9000).slideUp();
				}
			});
		}else{
			$('#upgrade_alert').html("Confirmation code can't be blank.").addClass('alert-danger').slideDown().delay(4000).slideUp();
		}
	});
	//scroll to top
	function scrollToItem(element){
		$('html, body').animate({
			scrollTop: $(element).offset().top - 60
		});
	}
	//show score
	function showScore(){
		total = 0;
		wrong = 0;
		//get total
		total = progress;
		//get wrong
		wrong = $('.wrong').length;
		//get right
		correct = total - wrong;
		$('#entity .score').html('You got '+correct+' out of '+total+' correct');
		$('#entity .scoresheet').slideDown();
	}
	//get login
	function onLogin(login){
		//get classrooms and update UI
		if(login.auth_token){
			$.getJSON(api_base_url+'classrooms.json?key='+login.auth_token, function(classrooms){
				$.each(classrooms, function(){ addItem('class', this); });
			});

			takeOut($('#login'));
			bringIn($('#classes'));
			$('.navbar').find('li.active').removeClass('active');
			$('.nav-home').parents('li').eq(0).addClass('active');
			$('.navbar-nav').fadeIn();
			$('#logout-nav').fadeIn();
			$('.navbar-toggle').css({display:''});

			//load subscription info
			$.getJSON(api_base_url+'subscriptions/plan_info.json?key='+login.auth_token, function(data){
				if(data.success){
					$('#subscription_info').html(data.message);
				}else{
					$('#subscription_info').html(data.error);
				}
			});
		}
	}
	//option click
	function optionSelect(){
		$(this).parents('.options').eq(0).children('.option').removeClass('selected');
		$(this).addClass('selected');
		//store answer and question id
		entity_id = $(this).parents('.entity').eq(0).data('id');
		chosen[entity_id] = $(this).data('value');
		//update progress
		if(typeof(progress) !== 'undefined'){
			$('.progress-wrapper').slideDown();
		}
		
		progress = 0;
		for(var key in chosen){
			if(chosen.hasOwnProperty(key)) progress++;
		}

		entity_max = $('.progress-bar').attr('aria-valuemax');
		
		$('#entity .progress-bar').attr("aria-valuenow", progress);
		$('#entity .progress-bar').css({"width": (progress/entity_max * 100)+"%"});
		$('#entity .complete').html(progress+'/'+entity_max);

		//make selections possible
		$('#entity .entities .option').css({'cursor':''});
	}
	//sync selections
	function syncSelections(){
		if($('#entity .option.right').length < 1){
			//submit choosen answers
			$.ajax({
				type:"POST",
				url:api_base_url+'people/'+login.person_id+'/sets/'+set_item.id+'/next?key='+login.auth_token,
				data:{ _method:"PUT", person_set:{chosen: JSON.stringify(chosen), progress: progress} }
			}).done(function(response){
				if(response.success){
					//show answers and hints
					$.each(response.entities, function(){
						$('#entity .entity[data-id="'+this.entity.id+'"] .option[data-value="'+this.entity.question_answer+'"]').addClass('right');
						$('#entity .entity[data-id="'+this.entity.id+'"] .comment').html(this.entity.comment);
						if(chosen[this.entity.id] != this.entity.question_answer){
							$('#entity .entity[data-id="'+this.entity.id+'"] .selected').addClass('wrong');
						}
					});
					showScore();
				}
			}).fail(function(response){
				//failed - do nothing or something

			});
		}
	}
	//auto sign in
	if(window.localStorage){
		if(localStorage.auth_token){
			login = {
				auth_token: localStorage.auth_token,
				email: localStorage.email,
				customer: localStorage.customer,
				person_sets_count: localStorage.person_sets_count,
				person_id: localStorage.person_id
			};
			onLogin(login);
		}
	}
	$(document).ready(function(){
		$('.recover').click(function(e) {
			e.preventDefault();
			window.open(api_base_url + 'secret/new', '_blank');
			// e.preventDefault();
			console.log('clicked');
		});
		$('.reverify').click(function(e) {
			e.preventDefault();
			window.open(api_base_url + 'verification/new', '_blank');
			// e.preventDefault();
			console.log('clicked');
		});
		$(document).on('click', '.exter', function(e){
		    e.preventDefault();
		    window.open(api_base_url + 'upgrading', '_blank');
		    console.log('clicked');
	    });
		
	});
		
});